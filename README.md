# Recursos

## Principios de Android
 - [contextneutral.com  Android Dev 101: Things every beginner should know](https://contextneutral.com/story/android-dev-101-things-every-beginner-must-know) ([Revisar](https://contextneutral.com/android))


## Sobre diseño / colores
- [material.io  The Color System ](https://material.io/design/color/the-color-system.html)
- [material.io  Applying Color to UI ](https://material.io/design/color/applying-color-to-ui.html)
- [material.io  Color Tool ](https://material.io/tools/color/)
- [material.io  Buttons](https://material.io/design/components/buttons.html#)
- [medium.co  Android: Working with themes and styles](https://medium.com/@joannekao/android-working-with-themes-and-styles-18cde717f4d)
- [developer.android.com Estilos y temas ](https://developer.android.com/guide/topics/ui/themes)
- [developer.android.com Iconos adaptativos](https://developer.android.com/guide/practices/ui_guidelines/icon_design_adaptive)


## Buenas prácticas
- [github.com/futurice  Buenas prácticas en el desarrollo de Android](https://github.com/futurice/android-best-practices/blob/master/translations/Spanish/README.es.md)
- [developer.android.com  Compatibilidad con diferentes pantallas](https://developer.android.com/guide/practices/screens_support) *prestar atención a Prácticas recomendadas*
- [javapractices.com  Package by feature, not layer](http://www.javapractices.com/topic/TopicAction.do?Id=205) **¿ordenar un proyecto por *capas* o or *funcionalidades*?**


## Patrones
- [proandroiddev.com  Introduction to Android Architecture Components](https://proandroiddev.com/introduction-to-android-architecture-components-cab33baa65f6)


## Consumo de datos
- [proandroiddev.com  Firebase Android Series: Learning Firebase from zero to hero](https://proandroiddev.com/firebase-android-series-learning-firebase-from-zero-to-hero-3bacbdf8e048) *revisar si vale la pena*
- [jackrutorial.com  Build an Android Application for User Login using Restful Web Services with Retrofit 2 Android Tutorial ](https://www.jackrutorial.com/2018/02/build-an-android-application-for-user-login-using-restful-web-services-with-retrofit-2-android-tutorial.html)


# Especificos
- [stackoverflow.com  Why is AppTheme.NoActionBar not working?](https://stackoverflow.com/questions/36478925/why-is-apptheme-noactionbar-not-working/36479167#36479126)
- [es.stackoverflow.com  Enviar datos entre activities](https://es.stackoverflow.com/questions/36902/enviar-datos-entre-activities)
- [geekytheory.com  Tutorial Android - 10. Paso de parámetros entre Activities ](https://geekytheory.com/tutorial-android-10-paso-de-parametros-entre-activities)
 

## Botones
- [stackoverflow.com  Android Material Design Button Styles](https://stackoverflow.com/questions/26346727/android-material-design-button-styles)
- [developer.android.com  Buttons](https://developer.android.com/guide/topics/ui/controls/button)
- [stackoverflow.com  How to make a background of a button transparent with visible border in Android?](https://stackoverflow.com/questions/44900496/how-to-make-a-background-of-a-button-transparent-with-visible-border-in-android)


## BottomNavigationView
- [medium.com  Exploring the Android Design Support Library: Bottom Navigation View](https://medium.com/@hitherejoe/exploring-the-android-design-support-library-bottom-navigation-drawer-548de699e8e0)
- [simplifiedcoding.net  Bottom Navigation Android Example using Fragments](https://www.simplifiedcoding.net/bottom-navigation-android-example/)


## Actiobar
- [stackoverflow.com  How to add Action bar options menu in Android Fragments](https://stackoverflow.com/a/31935887) para ajustarlo como boton falta agregar *app:showAsAction="always"*
- [stackoverflow.com  How to create button in Action bar in android](https://stackoverflow.com/questions/38158953/how-to-create-button-in-action-bar-in-android)


## Spinner
- [developer.android.com  Controles de números ](https://developer.android.com/guide/topics/ui/controls/spinner)
- [stackoverflow.com  How to set selected item of Spinner by value, not by position?](https://stackoverflow.com/a/4228121)


## Pickers: date/time picker
- [journaldev.com  Android Date Time Picker Dialog](https://www.journaldev.com/9976/android-date-time-picker-dialog)
- [developer.android.com  Pickers](https://developer.android.com/guide/topics/ui/controls/pickers)


## SQLite
- [codestart.info  Android SQLite tutorial with Recyclerview. (CRUD)](http://codestart.info/android-sqlite-tutorial-with-recyclerview-crud/) El ejemplo esta sobre una activity directamente y al hacer 'click' sobre la tarjeta abre un *AlertDialog*, algunas adaptaciones en los otros links de esta sección


## Room
- [codelabs.developers.google.com  Android Room with a View](https://codelabs.developers.google.com/codelabs/android-room-with-a-view/#0) **IMPORTANTE**


## RecyclerView y CardView
- [medium.com  Android FRAGMENT USE RecyclerView + CardView](https://medium.com/@Pang_Yao/android-fragment-use-recyclerview-cardview-4bc10beac446)
- [developer.android.com  Create a List with RecyclerView ](https://developer.android.com/guide/topics/ui/layout/recyclerview#java)
- [stackoverflow.com How can I set OnClickListener to two buttons in RecyclerView?](https://stackoverflow.com/a/45474550) *No emplee la mejor opcion. Esto se debe mejorar*


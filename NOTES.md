# Pendiente para revisar

## Rest API
 - [Token based authentication in REST APIs](https://stackoverflow.com/a/36279890)
 - [CodeIgniter RAP - CodeIgniter RESTful API](https://chalarangelo.github.io/codeigniter-rap/)
 - [A fully RESTful server implementation for CodeIgniter using one library, one config file and one controller.](https://github.com/chriskacerguis/codeigniter-restserver)
 - [A plain English introduction to JSON web tokens (JWT): what it is and what it isn’t](https://medium.com/ag-grid/a-plain-english-introduction-to-json-web-tokens-jwt-what-it-is-and-what-it-isnt-8076ca679843)
 - [How to create RESTful web services in codeigniter](http://programmerblog.net/create-restful-web-services-in-codeigniter/)
 - [Create A REST API In Codeigniter With Basic Authentication](https://dev.to/pardip_trivedi/create-a-rest-api-in-codeigniter-with-basic-authentication-1e7f)
 - [ycrao/tinyme Framework para API](https://github.com/ycrao/tinyme/tree/73064c7f3fb3fb5b91c0cef000613789b11ef328)